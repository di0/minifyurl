﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SimpleLinkShortenerService.Models
{
    /*
        Creating the sqlite database with EntityFrameworkCore and dotnet 
        terminal commands:
        - in the terminal, navigate to project folder
        - Run `dotnet ef migrations add InitialCreate` to scaffold a 
        migration and create the initial set of tables for the model.
            -  To undo this action, run `dotnet ef migrations remove`
        - Run `dotnet ef database update` to apply the new migration to the 
        database. This command creates the database before applying migrations.
        - The blogging.db SQLite DB is in the project directory.

        source: 
        https://docs.microsoft.com/en-us/ef/core/get-started/netcore/new-db-sqlite
     */
    public class LinkShorteningContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=d:\\blogging.db");
        }
    }

    public class Blog
    {
        public int BlogId { get; set; }
        public string Url { get; set; }

        public ICollection<Post> Posts { get; set; }
    }

    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public int BlogId { get; set; }
        public Blog Blog { get; set; }
    }
}
