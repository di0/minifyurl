﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using SimpleLinkShortenerService.Models;
using System;

namespace SimpleLinkShortenerService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            initializeDatabase();

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();

        private static void initializeDatabase()
        {
            using (var db = new LinkShorteningContext())
            {
                db.Blogs.Add(new Blog { Url = "http://blogs.msdn.com/adonet" });
                var count = db.SaveChanges();
                Console.WriteLine("{0} records saved to database", count);

                Console.WriteLine();
                Console.WriteLine("All blogs in database:");
                foreach (var blog in db.Blogs)
                {
                    Console.WriteLine(" - {0}", blog.Url);
                }
            }
        }
    }
}
